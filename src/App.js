
import './App.css';
import Navber from './componets/Navber';
import TextForm from './componets/TextForm';
import DarkMode from './componets/DarkMode';

function App() {
  return (
    <>
      <Navber title="Textutils" home="Home" about="About us" />
    <div className="container">
    <TextForm title="Enter your text here.." />
    <DarkMode title="this is dark mode" />
    </div>
    </>
  );
}

export default App;
