import React, { useState } from 'react'


export default function TextForm(props) {
    const [text, setText] = useState('');
    const [isBold, setIsBold] = useState(false);

  const toggleBold = () => {
    setIsBold(!isBold);
  };


    const toUpper = () => {
        let newText = text.toLocaleUpperCase();
        setText(newText);
    }
    const toLower = () => {
        let newText = text.toLocaleLowerCase();
        setText(newText);
    }
    const clearText = () => {
        let newText = "";
        setText(newText);
    }
    // const boldText = () => {
    //     let newText = text;
    //     setText(!newText);
    // }
    const handleOnChange = (event) => {
        setText(event.target.value);
    }

    return (
        <div>
            <div className='container'>
                <div className="form-group">
                    <label className="h3 form-label">{props.title}</label>
                    <textarea style={{ fontWeight: isBold ? 'bold' : 'normal' }}
                        className= "form-control my-3" aria-label="With textarea" row="8" value={text} placeholder="Enter your text here" onChange={handleOnChange}></textarea>
                    <div className="row">
                        <div className="col-3">
                            <button className="btn btn-primary " onClick={toUpper}>Convert To Uppercase</button>
                        </div>
                        <div className="col-3">
                            <button className="btn btn-secondary" onClick={toLower}>Convert To Lowercase</button>
                        </div>
                        <div className="col-3">
                            <button className="btn btn-danger" onClick={clearText}>Clear text</button>
                        </div>
                        <div className="col-3">
                            <button className="btn btn-dark" onClick={toggleBold}>made text in bold</button>
                        </div>

                    </div>
                </div>

            </div>
            <div className="container mt-4 row">
                <div className="col-6">
                    <h3>Entered text Summery</h3>
                    <p>{text.split(" ").length} Words and {text.length} Cherecters</p>
                </div>
                <div className="col-6">
                    <h3>Avg. time to take read text</h3>
                    <p>{0.008 * text.split(" ").length} Minetus take to read</p>
                </div>
                <div className="col-2">
                    <h3>Text Preview</h3>
                    <p>{text}</p>
                </div>
            </div>
        </div>
    )
}
