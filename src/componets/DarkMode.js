import React, { useState } from 'react'

export default function DarkMode(props) {
    const [myStyle, setMyStyle] = useState({
        color: 'black',
        backgroundColor: 'white'
      });
    
      const modeChange = () => {
        if (myStyle.color === 'black') {
          setMyStyle({
            color: 'white',
            backgroundColor: 'black'
          })
        }
        else {
          setMyStyle({
            color: 'black',
            backgroundColor: 'white'
          });
        }
      }
  return (
    <div className='row px-3'>
      <div className="col8" style={myStyle}>
        <h1 style={myStyle}>{props.title}</h1>
        <div className="col4">
        <button className="btn btn-outline-success" type="submit" onClick={modeChange}>Search</button>
        </div>
      </div>
    </div>
  )
}
